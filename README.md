# APoD: Astronomical Picture of (the) Day
Pulls the Astronomy Photo of (the) Day from the official NASA API and optionally sets it as your wallpaper

---

## Installation

The simplest way to install `apod` is through `cargo`, as follows:
```bash
cargo install apod
```

You can also install it on arch-based distros using the PKGBUILD, by downloading the PKGBUILD alone and running `makepkg`:
```bash
curl -LO https://git.tar.black/michal/apod/raw/branch/main/PKGBUILD
makepkg -rsic
```

Nix users with flake functionality enabled can also install `apod` using the following command:
```bash
nix profile install "git+https://git.tar.black/michal/apod#"
```

More installation options could come in the future. Who knows ¯\\_(ツ)\_/¯

## Usage

To be able to use this, you need an API key from [NASA](https://api.nasa.gov/) set as an environment variable called `NASA_API_KEY`.

You can add the following to your shell rc files:

**Bash/Zsh**
```
export NASA_API_KEY=<your-api-key>
```

**Fish**
```
set -Ux NASA_API_KEY <your-api-key>
```

**Powershell**
```
$env:NASA_API_KEY = "<your-api-key>"
```

### Global Arguments
| Argument          | Description                                              |
|-------------------|----------------------------------------------------------|
| `--verbose`, `-v` | Prints debug messages to stderr                          |
| `--low_def`, `-l` | Where applicable, uses the low-res JPEG form of the APoD |


### Subcommands

| Subcommand  | Description                                                                    |
|-------------|--------------------------------------------------------------------------------|
| `info`      | Gets the info of today's APoD and prints to stdout                             |
| `set`       | Sets the APoD as your wallpaper                                                |
| `download`  | Downloads the APoD to a directory of your choice                               |
| `install`   | Installs the APoD User Service, which sets your wallpaper to the APoD on login |
| `uninstall` | Uninstalls the APoD User Service                                               |

### Subcommand Arguments

#### `info`:

| Argument  | Description                                              |
|-----------|----------------------------------------------------------|
| `--long`  | Grabs the long APoD description, instead of a short-form |

#### `set`:

| Argument            | Description                                                                                 |
|---------------------|---------------------------------------------------------------------------------------------|
| `--low_def`, `-l`   | Sets wallpaper to the low-res JPEG form of the APoD                                         |
| `--mode`, `-m`      | Sets the wallpaper mode to one of: center, crop, fit, span, stretch, tile (crop by default) |
| `--no-notify`, `-n` | Disables the notification when the wallpaper is set                                         |

#### `download`:

| Argument          | Description                                                 |
|-------------------|-------------------------------------------------------------|
| `--low_def`, `-d` | Downloads the low-res JPEG form of the APoD                 |
| `--path`, `-p`    | Sets the directory to download the APoD to (PWD by default) |

#### `install`:

| Argument            | Description                                                                                 |
|---------------------|---------------------------------------------------------------------------------------------|
| `--low_def`, `-l`   | Installs the low-res JPEG form of the APoD User Service                                     |
| `--mode`, `-m`      | Sets the wallpaper mode to one of: center, crop, fit, span, stretch, tile (crop by default) |
| `--no-notify`, `-n` | Disables the notification when the wallpaper is set                                         |

#### `uninstall`:

No extra arguments.

# TODO:

- Implement `install` and `uninstall` for:
  - [x] systemd
  - [ ] runit
  - [ ] macOS
  - [x] Windows