use clap::Parser;

use crate::operation::{download, query, set_wallpaper};

mod args;
mod internal;
mod operation;

fn main() {
    let args = args::Args::parse();
    match args.subcommand.as_ref().unwrap() {
        args::Operation::Query { long } => {
            query(args::Args::parse(), *long);
        }
        args::Operation::Download { path } => {
            download(args::Args::parse(), path.to_string());
        }
        args::Operation::Set { mode, no_notify } => {
            set_wallpaper(args::Args::parse(), mode.to_string(), *no_notify);
        }
        args::Operation::Install { mode, no_notify } => {
            operation::install(args::Args::parse(), mode.to_string(), *no_notify);
        }
        args::Operation::Uninstall => {
            operation::uninstall(args::Args::parse());
        }
    }
}
