use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Apod {
    pub title: String,
    pub explanation: String,
    pub url: String,
    pub hdurl: Option<String>,
    pub media_type: String,
    pub service_version: String,
    pub date: String,
    pub copyright: Option<String>,
}
