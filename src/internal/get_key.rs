pub fn get_key() -> String {
    std::env::var("NASA_API_KEY").unwrap_or_else(|e| {
        eprintln!("Failed to get NASA API key: {}", e);
        std::process::exit(1);
    })
}
