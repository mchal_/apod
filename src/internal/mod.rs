use std::sync::Arc;

pub use consts::*;
use get_key::*;
pub use structs::*;

use crate::args::Args;

mod consts;
mod get_key;
mod structs;

pub fn query(args: Args) -> Apod {
    if args.verbose {
        eprintln!("Querying APoD API...");
    }

    let api_key = get_key();
    if args.verbose {
        eprintln!("API Key: {}", api_key);
    }

    let url = format!("{}?api_key={}", API_URL, api_key);
    if args.verbose {
        eprintln!("URL: {}", url);
    }

    if args.verbose {
        eprintln!("Querying...");
    }
    let tls_connector = Arc::new(native_tls::TlsConnector::new().unwrap_or_else(|e| {
        eprintln!("Failed to create native TLS connector: {}", e);
        std::process::exit(1);
    }));

    let agent = ureq::AgentBuilder::new()
        .tls_connector(tls_connector)
        .build();

    let apod = agent
        .get(&url)
        .call()
        .unwrap_or_else(|e| {
            eprintln!("Failed to query APoD API: {}", e);
            std::process::exit(1);
        })
        .into_json::<Apod>()
        .unwrap_or_else(|e| {
            eprintln!("Failed to parse JSON: {}", e);
            std::process::exit(1);
        });

    if apod.service_version != "v1" {
        eprintln!("Service version has changed, here be dragons!");
    }

    if args.verbose {
        eprintln!("Querying complete.");
    }

    apod
}
