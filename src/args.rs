use clap::{ArgAction, Parser, Subcommand};

#[derive(Debug, Clone, Parser)]
#[clap(name = env ! ("CARGO_PKG_NAME"), version = env ! ("CARGO_PKG_VERSION"), author = env ! ("CARGO_PKG_AUTHORS"), about = env ! ("CARGO_PKG_DESCRIPTION"), settings = & [clap::AppSettings::SubcommandRequiredElseHelp])]
pub struct Args {
    #[clap(subcommand)]
    pub subcommand: Option<Operation>,

    /// Whether or not to print debug messages
    #[clap(short, long, action = ArgAction::SetTrue, global = true)]
    pub verbose: bool,

    /// Whether to grab the low-definition JPEG instead of the high-definition PNG
    #[clap(short, long, action = ArgAction::SetTrue, global = true)]
    pub low_def: bool,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Operation {
    /// Returns info about today's APOD
    #[clap(name = "info")]
    Query {
        /// Whether to show the long description, instead of only the first sentence
        #[clap(long)]
        long: bool,
    },

    /// Downloads today's APOD to a directory of your choice
    #[clap(name = "download")]
    Download {
        /// The directory to download the image to
        #[clap(short, long, default_value = "./")]
        path: String,
    },

    /// Sets the wallpaper to today's APOD
    #[clap(name = "set")]
    Set {
        /// The mode to set the wallpaper in
        #[clap(short, long, default_value = "crop", possible_values = & ["center", "crop", "fit", "span", "stretch", "tile"])]
        mode: String,

        /// Whether to suppress notification when the wallpaper is set
        #[clap(short, long, action = ArgAction::SetTrue, global = true)]
        no_notify: bool,
    },

    /// Installs the APOD wallpaper user-service (systemd only)
    #[clap(name = "install")]
    Install {
        /// The mode to set the wallpaper in
        #[clap(short, long, default_value = "crop", possible_values = & ["center", "crop", "fit", "span", "stretch", "tile"])]
        mode: String,

        /// Whether to suppress notification when the wallpaper is set
        #[clap(short, long, action = ArgAction::SetTrue, global = true)]
        no_notify: bool,
    },

    /// Uninstalls the APOD wallpaper user-service (systemd only)
    #[clap(name = "uninstall")]
    Uninstall,
}
