#[cfg(target_os = "linux")]
use std::process::Command;

use colourizer::StyledString;

use crate::args::Args;

#[cfg(target_os = "macos")]
pub fn uninstall(args: Args) {
    println!("{}", "Installing APoD Wallpaper User Service".bold());
    let service_name = "Astronomy Picture of (the) Day.sh";
    let service_path = format!(
        "{}/{}",
        dirs::home_dir().unwrap().to_str().unwrap(),
        service_name
    );

    if args.verbose {
        eprintln!("Removing shortcut: {}", service_path);
    }
    if std::path::Path::new(&service_path).exists() {
        std::fs::remove_file(&service_path).unwrap_or_else(|e| {
            eprintln!("Failed to remove file: {}", e);
            std::process::exit(1);
        });
    }
}

#[cfg(target_os = "windows")]
pub fn uninstall(args: Args) {
    println!("{}", "Uninstalling APoD Wallpaper User Service".bold());
    let service_name = "apod.lnk";
    let service_path_dir = format!(
        "{}\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\",
        dirs::home_dir().unwrap().to_str().unwrap()
    );
    let service_path = format!("{}{}", &service_path_dir, service_name);
    if args.verbose {
        eprintln!("Removing shortcut: {}", service_path);
    }

    if std::path::Path::new(&service_path).exists() {
        std::fs::remove_file(&service_path).unwrap_or_else(|e| {
            eprintln!("Failed to remove file: {}", e);
            std::process::exit(1);
        });
    }
}

#[cfg(target_os = "linux")]
pub fn uninstall(args: Args) {
    println!("{}", "Uninstalling APoD Wallpaper User Service".bold());
    if args.verbose {
        eprintln!("Getting user info");
    }
    let user = whoami::username();

    let service_name = format!("apod-wallpaper-{}.service", user);
    let service_path = format!(
        "{}/.local/share/systemd/user/{}",
        dirs::home_dir().unwrap().to_str().unwrap(),
        service_name
    );

    if args.verbose {
        eprintln!("Disabling service: {}", service_name);
    }
    Command::new("systemctl")
        .arg("--user")
        .arg("disable")
        .arg("--now")
        .arg(&service_name)
        .output()
        .unwrap_or_else(|e| {
            eprintln!("Failed to disable service: {}", e);
            std::process::exit(1);
        });

    if args.verbose {
        eprintln!("Removing service: {}", service_name);
    }
    std::fs::remove_file(&service_path).unwrap_or_else(|e| {
        eprintln!("Failed to remove service: {}", e);
        std::process::exit(1);
    });
}
