use notify_rust::{Notification, Timeout};
use wallpaper::set_from_url;

#[cfg(not(target_os = "macos"))]
use wallpaper::set_mode;

use crate::args::Args;
use crate::internal::query;

#[allow(unused_variables)]
pub fn set_wallpaper(args: Args, mode: String, no_notify: bool) {
    if args.verbose {
        eprintln!("Setting wallpaper...");
    }

    let apod = query(args.clone());

    let url = if args.low_def {
        apod.url
    } else if let Some(hdurl) = apod.hdurl {
        hdurl
    } else {
        apod.url
    };

    if apod.media_type == "video" {
        eprintln!("Videos and other formats are not supported by APoD.");
        return;
    }

    set_from_url(&url).unwrap_or_else(|e| {
        eprintln!("Failed to set wallpaper: {}", e);
        std::process::exit(1);
    });

    #[cfg(not(target_os = "macos"))]
    match mode.as_str() {
        "center" => set_mode(wallpaper::Mode::Center),
        "crop" => set_mode(wallpaper::Mode::Crop),
        "fit" => set_mode(wallpaper::Mode::Fit),
        "span" => set_mode(wallpaper::Mode::Span),
        "stretch" => set_mode(wallpaper::Mode::Stretch),
        "tile" => set_mode(wallpaper::Mode::Tile),
        _ => {
            eprintln!("Invalid mode: {}", mode);
            std::process::exit(1);
        }
    }
    .unwrap_or_else(|e| {
        eprintln!("Failed to set wallpaper mode: {}", e);
        std::process::exit(1);
    });

    if !no_notify {
        #[cfg(target_os = "linux")]
        Notification::new()
            .summary(&*format!("APoD {}: {}", apod.date, apod.title))
            .body(&*format!(
                "{}...",
                apod.explanation.split('.').next().unwrap()
            ))
            .action("clicked", "Read More")
            .timeout(Timeout::Milliseconds(20000))
            .show()
            .unwrap_or_else(|e| {
                eprintln!("Failed to show notification: {}", e);
                std::process::exit(1);
            })
            .wait_for_action(|act| {
                if act == "clicked" {
                    open::that("https://apod.nasa.gov/apod/astropix.html").unwrap_or_else(|e| {
                        eprintln!("Failed to open URL: {}", e);
                        std::process::exit(1);
                    });
                }
            });

        #[cfg(any(target_os = "macos", target_os = "windows"))]
        Notification::new()
            .summary(&format!("APoD {}: {}", apod.date, apod.title))
            .body(&format!(
                "{}...",
                apod.explanation.split('.').next().unwrap()
            ))
            .timeout(Timeout::Milliseconds(20000))
            .show()
            .unwrap_or_else(|e| {
                eprintln!("Failed to show notification: {}", e);
                std::process::exit(1);
            });
    };
}
