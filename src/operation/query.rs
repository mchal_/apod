use colourizer::StyledString;

use crate::args::Args;
use crate::internal;

pub fn query(args: Args, long: bool) {
    let apod = internal::query(args);

    let desc = if long {
        apod.explanation.clone()
    } else {
        apod.explanation.split('.').next().unwrap_or("").to_string()
    };

    println!("{} {}", "Title:".bold().fgwhite(), apod.title);
    println!("{} {}", "Explanation:".bold().fgwhite(), desc);
    println!("{} {}", "URL:".bold().fgwhite(), apod.url);
    println!(
        "{} {}",
        "HD URL:".bold().fgwhite(),
        apod.hdurl.unwrap_or("N/A".to_string())
    );
    println!("{} {}", "Date:".bold().fgwhite(), apod.date);
    println!(
        "{} {}",
        "Copyright:".bold().fgwhite(),
        apod.copyright.unwrap_or_else(|| "None".parse().unwrap())
    );
}
