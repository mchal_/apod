use std::io::Write;
use std::path::Path;
use std::sync::Arc;

use colourizer::StyledString;

use crate::args::Args;
use crate::internal;

pub fn download(args: Args, path: String) {
    if args.verbose {
        eprintln!("Querying APoD Info");
    }
    let apod = internal::query(args.clone());
    let url = if args.low_def {
        apod.url
    } else if let Some(hdurl) = apod.hdurl {
        hdurl
    } else {
        apod.url
    };

    let path_string = format!(
        "{}/APoD-{}-{}",
        path,
        apod.date,
        if args.low_def { "SD.jpg" } else { "HD.png" }
    );
    let path = Path::new(&path_string);

    if args.verbose {
        eprintln!("Creating empty file to write to");
    }
    let mut file = std::fs::File::create(path).unwrap_or_else(|e| {
        eprintln!("Failed to create file: {}", e);
        std::process::exit(1);
    });

    if args.verbose {
        eprintln!("Downloading image");
    }

    println!(
        "Downloading {} APoD for {}: \"{}\"",
        if args.low_def {
            "low-definition"
        } else {
            "high-definition"
        },
        apod.date.bold().fgwhite(),
        apod.title.bold().fgwhite()
    );

    let agent = ureq::AgentBuilder::new()
        .tls_connector(Arc::new(native_tls::TlsConnector::new().unwrap_or_else(
            |e| {
                eprintln!("Failed to create native TLS connector: {}", e);
                std::process::exit(1);
            },
        )))
        .build();

    let response = agent.get(&url).call().unwrap_or_else(|e| {
        eprintln!("Failed to query APoD API: {}", e);
        std::process::exit(1);
    });

    let mut buffer = Vec::new();
    response
        .into_reader()
        .read_to_end(&mut buffer)
        .unwrap_or_else(|e| {
            eprintln!("Failed to read response: {}", e);
            std::process::exit(1);
        });

    file.write_all(&buffer).unwrap_or_else(|e| {
        eprintln!("Failed to write to file: {}", e);
        std::process::exit(1);
    });
}
